package ksyen.com.imgloadtest;

import android.Manifest;
import android.app.usage.UsageEvents;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 1;

    private final String testLink = "https://st.kp.yandex.net/images/film_big/714888.jpg";
    private ImageView mainImg;
    private EditText mLikUrl;
    private TextInputLayout mLikUrlTxtLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainImg = (ImageView) findViewById(R.id.main_img);
        initButtons();
        initFields();
    }

    private void initButtons() {
        Button loadImgBtn = (Button) findViewById(R.id.load_img_btn);
        loadImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (canLoad()) {
                    mainImg.setImageBitmap(null);
                    startLoadService();
                }
            }
        });
    }

    private void startLoadService() {
        Intent intent = new Intent(MainActivity.this, LoadService.class);
        intent.putExtra(LoadService.FILE_NAME, "test.jpg");
        intent.putExtra(LoadService.PATH_URL,
                mLikUrl.getText().toString().trim());
        startService(intent);
    }

    private void initFields() {
        mLikUrl = (EditText) findViewById(R.id.link_txt);
        mLikUrlTxtLayout = (TextInputLayout) findViewById(R.id.link_txt_layout);

        mLikUrl.setText(testLink);
        mLikUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    mLikUrlTxtLayout.setErrorEnabled(false);
                }            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private boolean canLoad() {

        boolean canLoad = true;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            canLoad = false;
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        }

        if (mLikUrl.getText().toString().isEmpty()) {
            mLikUrlTxtLayout.setError(getString(R.string.main_activity_error));
            canLoad = false;
        }

        return canLoad;
    }

    @Subscribe
    public void onImageLoaded(@NonNull OnImageLoadedEvent event) {
        String filePath = event.getFilePath();
        int codeResult = event.getResult();
        if (codeResult == RESULT_OK) {
            Toast.makeText(MainActivity.this, "Load complited", Toast.LENGTH_LONG).show();
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            mainImg.setImageBitmap(bitmap);
            mainImg.setRotation(180.00f);
        }else {
            Toast.makeText(MainActivity.this, "Load failed", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (canLoad()) {
                    startLoadService();
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
