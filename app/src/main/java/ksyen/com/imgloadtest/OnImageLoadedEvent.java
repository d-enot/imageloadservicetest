package ksyen.com.imgloadtest;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OnImageLoadedEvent {

    private String filePath;
    private int    result;
}
