package ksyen.com.imgloadtest;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class LoadService extends IntentService {

    private int result = Activity.RESULT_CANCELED;

    public static final String PATH_URL = "path_url";
    public static final String FILE_NAME = "file_name";

    public LoadService() {
        super("LoadImgService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String pathUrl = intent.getStringExtra(PATH_URL);
        String fileName = intent.getStringExtra(FILE_NAME);

        File file = new File(Environment.getExternalStorageDirectory(), fileName);

        if (file.exists()) {
            file.delete();
        }

        InputStream stream = null;
        FileOutputStream out = null;

        try {
            URL url = new URL(pathUrl);
            stream = url.openConnection().getInputStream();
            out = new FileOutputStream(file.getPath());

            int read;
            byte[] data = new byte[1024];
            while ((read = stream.read(data)) != -1) {
                out.write(data,0,read);
            }
            result = Activity.RESULT_OK;
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        publishResults(file.getAbsolutePath(), result);
    }

    private void publishResults(@Nullable String filePath, int result) {
        EventBus.getDefault().post(new OnImageLoadedEvent(filePath, result));
    }
}
